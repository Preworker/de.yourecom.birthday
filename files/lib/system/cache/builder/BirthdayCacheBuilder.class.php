<?php
namespace wbb\system\cache\builder;
use wcf\system\cache\builder\AbstractCacheBuilder;
use wcf\data\user\User;
use wcf\system\database\util\PreparedStatementConditionBuilder;
use wcf\util\DateUtil;
use wcf\system\WCF;

/**
 * assign birthday infos to template
 *
 * @author 	Marcel Beckers
 * @license	Beerware <http://de.wikipedia.org/wiki/Beerware>
 * @package	de.yourecom.birthday
 */
class BirthdayCacheBuilder extends AbstractCacheBuilder {
	/**
	 * @see	\wcf\system\cache\builder\AbstractCacheBuilder::$maxLifetime
	 */
	protected $maxLifetime = 3600;
	
	/**
	 * @see	wcf\system\cache\builder\AbstractCacheBuilder::rebuild()
	 */
	public function rebuild(array $parameters) {
		$data = array('today' => array(), 'future' => array());
		
		$optionIDBirthday = User::getUserOptionID('birthday');
		$timestamp = TIME_NOW - BIRTHDAY_LASTACTIVITY * 60 * 60 *24;
		
		//birthdays of the curent day
		$formatedTime = DateUtil::getDateTimeByTimestamp(TIME_NOW);
		$sql = "SELECT		user_options.userID, user_options.userOption".$optionIDBirthday." , user.username, user.lastActivityTime
				FROM		wcf".WCF_N."_user_option_value user_options
				LEFT JOIN	wcf".WCF_N."_user user
					ON (user.userID = user_options.userID)
				WHERE user_options.userOption".$optionIDBirthday." LIKE '____-".$formatedTime->format('m-d')."'
					AND user.lastActivityTime >= ?";
		$statement = WCF::getDB()->prepareStatement($sql);
		$statement->execute(array(
			$timestamp
		));
		while ($row = $statement->fetchArray()) {
			$row['age'] = date('Y') - substr($row['userOption'.$optionIDBirthday], 0, 4);
			
			if($row['lastActivityTime'] >= $timestamp) {
				$data['today'][] = $row;
			}
		}
		
		//birthdays of the future days
		if(BIRTHDAY_NEXTDAYSENABLE) {
			$additionalSql = '';
			
			for ($i = 1; $i <= BIRTHDAY_NEXTDAYS; $i++) {
				$future = DateUtil::getDateTimeByTimestamp(TIME_NOW);
				$future->modify('+'.$i.' day');
				
				if(!empty($additionalSql)) $additionalSql .= ' OR ';
				$additionalSql .= "user_options.userOption".$optionIDBirthday." LIKE '____-".$future->format('m-d')."'";
			}
			
			$sql = "SELECT		user_options.userID, user_options.userOption".$optionIDBirthday." , user.username, user.lastActivityTime
					FROM		wcf".WCF_N."_user_option_value user_options
					LEFT JOIN	wcf".WCF_N."_user user
						ON (user.userID = user_options.userID)
					WHERE	".$additionalSql."
						AND user.lastActivityTime >= ".intval($timestamp);
			$statement = WCF::getDB()->prepareStatement($sql);
			$statement->execute();
			while ($row = $statement->fetchArray()) {
				if($row['lastActivityTime'] >= $timestamp) {
					$data['future'][] = array(
						'username' => $row['username'],
						'userID' => $row['userID'],
						'age' => date('Y') - substr($row['userOption'.$optionIDBirthday], 0, 4),
						'days' => $this->seDay(date('Y').substr($row['userOption'.$optionIDBirthday], 4), date("Y-m-d"), "Ymd","-")
					);
				}
			}
		}
		return $data;
	}
	
	public function seDay($begin, $end, $format, $sep){
		$pos1 = strpos($format, 'd');
		$pos2 = strpos($format, 'm');
		$pos3 = strpos($format, 'Y');
		
		$begin = explode($sep,$begin);
		$end = explode($sep,$end);
		
		$first = GregorianToJD($end[$pos2],$end[$pos1],$end[$pos3]);
		$second = GregorianToJD($begin[$pos2],$begin[$pos1],$begin[$pos3]);
		
		if($first > $second) {
			return $first - $second;
		}
		else {
			return $second - $first;
		}
	}
}