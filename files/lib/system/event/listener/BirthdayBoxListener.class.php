<?php
namespace wbb\system\event\listener;
use wbb\system\cache\builder\BirthdayCacheBuilder;
use wcf\system\event\IEventListener;
use wcf\system\WCF;

/**
 * assign birthday infos to template
 *
 * @author 	Marcel Beckers
 * @license	Beerware <http://de.wikipedia.org/wiki/Beerware>
 * @package	de.yourecom.birthday
 */
class BirthdayBoxListener implements IEventListener {
	
	public function execute($eventObj, $className, $eventName) {
		if (BIRTHDAY_ENABLE) {
			$birthdays = BirthdayCacheBuilder::getInstance()->getData();
			if (count($birthdays) > 0) {
				WCF::getTPL()->append('birthdayData', $birthdays);
			}
		}
	}
}