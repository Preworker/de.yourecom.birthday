{if BIRTHDAY_ENABLE && $__wcf->getSession()->getPermission('user.board.canViewBirthday') && ($birthdayData.today|count || $birthdayData.future|count)}
	<li class="box32 bithdayInfoBox">
		<span class="icon icon32 icon-gift"></span>
		<div>
			<div class="containerHeadline">
				<h3>{lang}wbb.index.birthday{/lang}</h3>
				{if $birthdayData.today|count}
					<p class="smallFont">{lang}wbb.index.birthday.description{/lang}</p>
					<p class="smallFont">{implode from=$birthdayData.today item=birthday}<a href="{link controller='User' id=$birthday.userID}{/link}" class="userLink" data-user-id="{@$birthday.userID}">{@$birthday.username} ({@$birthday.age})</a> {/implode}</p>
				{/if}
				{if $birthdayData.future|count}
					<p class="smallFont">{lang}wbb.index.birthday.future.description{/lang}</p>
					<p class="smallFont">{implode from=$birthdayData.future item=birthday}<a href="{link controller='User' id=$birthday.userID}{/link}" class="userLink" data-user-id="{@$birthday.userID}">{@$birthday.username} ({@$birthday.age} {lang}wbb.index.birthday.years{/lang} noch {@$birthday.days} Tag{if $birthday.days > 1}e{/if})</a> {/implode}</p>
				{/if}
			</div>
		</div>
	</li>
{/if}